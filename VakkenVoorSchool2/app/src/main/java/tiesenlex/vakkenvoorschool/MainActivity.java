package tiesenlex.vakkenvoorschool;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Arrays;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {

    // HIER WORDEN EEN AANTAL VARIABELEN AANGEMAAKT:

    // DIT ZIJN DE TITELS DIE WE GEBRUIKEN IN DE HOMEPAGE LISTVIEW
    String[] delen = {"Algemene vakken", "Specialisatie vakken", "Keuze vakken", "Minor en Stage", "Afstudeerproject"};

    // DIT IS EEN ARRAYLIST WAARIN WIJ ONZE DATA VAN DE API IN KUNNEN ZETTEN
    ArrayList<Integer> totaalGehaald = new ArrayList<Integer>();

    // DIT ZIJN HET MAXIMAAL AANTAL HAALBARE PUNTEN PER VAKKENPAKKET
    String[] punten = {" / 24", " / 54", " / 12", " / 60", " / 30"};

    // IN DEZE VARIABELEN WORDEN DE TOTAAL BEHAALDE PUNTEN OPGESLAGEN
    int va, vs, vk, ms, af;

    // ONCREATE SPUL, WANT JA, IS NODIG ENZO
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // DE MAINLIST VIEW PAKKEN OM TE VULLEN MET CARDS
        final ListView mainList = (ListView) findViewById(R.id.mainList);

        // TOOLBAR VOOR DE APP
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // REQUESTQUEUE STARTEN
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "http://lexvanderwerff.com/api/imtpmd";

        // REQUEST STRING VAN DE URL
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>() {
            @Override
            public void onResponse(String response) {
                if (response != null) {
                    try {
                        // DATA DIE TERUG KOMT NAAR JSONARRAY DOEN
                        JSONArray alleData = new JSONArray(response);

                        // DOOR DE DATA HEEN LOOPEN
                        for (int i = 0; i < alleData.length(); i++) {
                            // VOOR IEDERE ROW IN DE DATA EEN JSONOBJECT VULLEN
                            JSONObject enkeleData = alleData.getJSONObject(i);

                            // KIJKEN OF HET BEHAALDE CIJFER HOGER IS DAN EEN 5.5
                            // ALS DAT ZO IS, AAN TOTAAL BEHAALDE PUNTEN VARIABELE TOEVOEGEN
                            if(enkeleData.getDouble("cijfer") >= 5.5 ){
                                switch(enkeleData.getInt("deel"))
                                {
                                    case 1:
                                        va = va + enkeleData.getInt("waarde");
                                        break;
                                    case 2:
                                        vs = vs + enkeleData.getInt("waarde");
                                        break;
                                    case 3:
                                        vk = vk + enkeleData.getInt("waarde");
                                        break;
                                    case 4:
                                        ms = ms + enkeleData.getInt("waarde");
                                        break;
                                    case 5:
                                        af = af + enkeleData.getInt("waarde");
                                        break;
                                }
                            }
                        }

                        // DE ARRAYLIST VULLEN MET DE TOTAAL BEHAALDE PUNTEN
                        totaalGehaald.addAll(Arrays.asList(va, vs, vk, ms, af));

                        // ADAPTER AAN DE MAINLIST KOPPELEN ZODAT IE MET CARDS GEVULD KAN WORDEN
                        CustomAdapter mainListAdapter = new CustomAdapter();
                        mainList.setAdapter(mainListAdapter);

                    } catch (final JSONException e) {
                        System.out.println("Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "Json parsing error: " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    System.out.println("Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Couldn't get json from server. Check LogCat for possible errors!",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
            Toast.makeText(getApplicationContext(), "That didn't work!", Toast.LENGTH_SHORT).show();
            }
        });

        // ADD THE REQUEST
        queue.add(stringRequest);

        // DE LIST VAN CARDS CLICKABLE MAKEN, ALS KLIK, DAN NAAR DAT VAKKENPAKKET
        mainList.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                Toast.makeText(getApplicationContext(), "Click ListItem Number " + position, Toast.LENGTH_SHORT).show();
                switch(position)
                {
                    case 0:
                        Intent intent0 = new Intent (view.getContext(), VakkenAlgemeen.class);
                        startActivityForResult(intent0, 0);
                        break;
                    case 1:
                        Intent intent1 = new Intent (view.getContext(), VakkenSpecialisatie.class);
                        startActivityForResult(intent1, 0);
                        break;
                    case 2:
                        Intent intent2 = new Intent (view.getContext(), VakkenKeuze.class);
                        startActivityForResult(intent2, 0);
                        break;
                    case 3:
                        Intent intent3 = new Intent (view.getContext(), MinorStage.class);
                        startActivityForResult(intent3, 0);
                        break;
                    case 4:
                        Intent intent4 = new Intent (view.getContext(), Afstudeer.class);
                        startActivityForResult(intent4, 0);
                        break;

                }
            }
        });

        // DIT IS HET HAMBURGER MENU
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);
    }

    // DE ADAPTER DIE DE LIST VULT MET CARDS EN DATA UIT DE API
    class CustomAdapter extends BaseAdapter {

        @Override
        public int getCount() {
            return delen.length;
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            // CUSTOM LAYOUT INFLATEN, WELKE EEN CARD IS
            convertView = getLayoutInflater().inflate(R.layout.custom_layout, null);

            // TEXTVIEWS OPHALEN UIT CUSTOM_LAYOUT XML, ZODAT DE DATA ERIN KAN
            TextView textView_deel = (TextView) convertView.findViewById(R.id.textView_deel);
            TextView textView_behaald = (TextView) convertView.findViewById(R.id.textView_behaald);
            TextView textView_punten = (TextView) convertView.findViewById(R.id.textView_punten);

            // TEXTVIEWS VULLEN MET DATA
            textView_deel.setText(delen[position]);
            textView_behaald.setText(totaalGehaald.get(position).toString());
            textView_punten.setText(punten[position]);

            return convertView;
        }
    }

    // DIT IS VOOR DE HAMBURGER
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    // HET H
    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();
        Intent intent = null;

        if (id == R.id.nav_vakkenAlgemeen) {
            intent = new Intent(this, VakkenAlgemeen.class);
            startActivity(intent);
        } else if (id == R.id.nav_keuzeVakken) {
            intent = new Intent(this, VakkenKeuze.class);
            startActivity(intent);
        } else if (id == R.id.nav_vakkenSpecialisatie) {
            intent = new Intent(this, VakkenSpecialisatie.class);
            startActivity(intent);
        } else if (id == R.id.nav_minorStage) {
            intent = new Intent(this, MinorStage.class);
            startActivity(intent);
        } else if (id == R.id.nav_afstudeer) {
            intent = new Intent(this, Afstudeer.class);
            startActivity(intent);
        }

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
