//package tiesenlex.vakkenvoorschool;
//
//import android.content.Context;
//import android.view.LayoutInflater;
//import android.view.View;
//import android.view.ViewGroup;
//import android.widget.BaseAdapter;
//import android.widget.TextView;
//
//public class VakkenListAdapter extends BaseAdapter{
//
//    String[] vakken;
//    String[] waarden;
//    String[] cijfer;
//
//    Context context;
//
//    LayoutInflater layoutInflater;
//
//
//    public VakkenListAdapter(String[] vakken, String[] waarden, String[] cijfer, Context context) {
//        super();
//        this.vakken = vakken;
//        this.waarden = waarden;
//        this.cijfer = cijfer;
//
//        this.context = context;
//        layoutInflater = LayoutInflater.from(context);
//    }
//
//    @Override
//    public int getCount() {
//        return vakken.length;
//    }
//
//    @Override
//    public Object getItem(int position) {
//        return null;
//    }
//
//    @Override
//    public long getItemId(int position) {
//        return position;
//    }
//
//    @Override
//    public View getView(int position, View convertView, ViewGroup parent) {
//
//        convertView = layoutInflater.inflate(R.layout.custom_layout, null);
//
//        TextView textView_deel = (TextView) convertView.findViewById(R.id.textView_deel);
//        TextView textView_behaald = (TextView) convertView.findViewById(R.id.textView_behaald);
//        TextView textView_punten = (TextView) convertView.findViewById(R.id.textView_punten);
//
//        textView_deel.setText(vakken[position]);
//        textView_behaald.setText(waarden[position]);
//        textView_punten.setText(cijfer[position]);
//
//        return convertView;
//    }
//
//}