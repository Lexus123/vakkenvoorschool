package tiesenlex.vakkenvoorschool;

import android.content.Context;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.StringRequest;
import com.android.volley.toolbox.Volley;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class VakkenKeuze extends AppCompatActivity {

    ArrayList<String> vak = new ArrayList<String>();
    ArrayList<Integer> punten = new ArrayList<Integer>();
    ArrayList<Double> cijfer = new ArrayList<Double>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_vakken_keuze);

        final ListView vakkenList = (ListView) findViewById(R.id.vakkenList);

        // REQUESTQUEUE STARTEN
        RequestQueue queue = Volley.newRequestQueue(this);
        final String url = "http://lexvanderwerff.com/api/imtpmd";

        // REQUEST STRING VAN DE URL
        final StringRequest stringRequest = new StringRequest(Request.Method.GET, url, new Response.Listener<String>()
        {
            @Override
            public void onResponse(String response)
            {
                if (response != null)
                {
                    try
                    {
                        // DATA DIE TERUG KOMT NAAR JSONARRAY DOEN
                        JSONArray alleData = new JSONArray(response);

                        // DOOR DE DATA HEEN LOOPEN
                        for (int i = 0; i < alleData.length(); i++)
                        {
                            // VOOR IEDERE ROW IN DE DATA EEN JSONOBJECT VULLEN
                            JSONObject enkeleData = alleData.getJSONObject(i);

                            if(enkeleData.getInt("deel") == 3)
                            {
                                vak.add(enkeleData.getString("vak"));
                                punten.add(enkeleData.getInt("waarde"));
                                cijfer.add(enkeleData.getDouble("cijfer"));
                            }
                        }

                        System.out.println(vak);

                        // ADAPTER AAN DE MAINLIST KOPPELEN ZODAT IE MET CARDS GEVULD KAN WORDEN
                        VakkenListAdapter vakkenListAdapter = new VakkenListAdapter();
                        vakkenList.setAdapter(vakkenListAdapter);

                    } catch (final JSONException e) {
                        System.out.println("Json parsing error: " + e.getMessage());
                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                Toast.makeText(getApplicationContext(),
                                        "Json parsing error: " + e.getMessage(),
                                        Toast.LENGTH_LONG).show();
                            }
                        });
                    }
                } else {
                    System.out.println("Couldn't get json from server.");
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Toast.makeText(getApplicationContext(),
                                    "Couldn't get json from server. Check LogCat for possible errors!",
                                    Toast.LENGTH_LONG).show();
                        }
                    });
                }
            }
        }, new Response.ErrorListener()
        {
            @Override
            public void onErrorResponse(VolleyError error) {
                Toast.makeText(getApplicationContext(), "That didn't work!", Toast.LENGTH_SHORT).show();
            }
        });

        // ADD THE REQUEST
        queue.add(stringRequest);
    }

    class VakkenListAdapter extends BaseAdapter {
        @Override
        public int getCount() {
            return vak.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {
            convertView = getLayoutInflater().inflate(R.layout.custom_list, null);

            TextView textView_vak = (TextView) convertView.findViewById(R.id.textView_vak);
            TextView textView_waard = (TextView) convertView.findViewById(R.id.textView_waard);
            EditText editCijfer = (EditText) convertView.findViewById(R.id.editCijfer);

            textView_vak.setText(vak.get(position).toString());
            textView_waard.setText(punten.get(position).toString());
            editCijfer.setText(cijfer.get(position).toString());

            return convertView;
        }
    }
}
